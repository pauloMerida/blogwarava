import React from 'react';
import Modal from 'react-modal';
import './modal.css';
import Api from './Api';

export class ModalComponent extends React.Component{
 
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,coments:[] };
     }

  retornoComments=()=>{
    Api.posts.getComments(this.props.idPosts)
    .then(res =>{
    this.setState({
    coments:res
    });
    })
    .catch(error =>{
      console.log(error)
      })
  }

  openModal = () => {
    this.setState({modalIsOpen: true});
   }
 
  handleModalCloseRequest = () => {
    this.setState({modalIsOpen: false});
   }
    
  handleSaveClicked = (e) => {
    alert('Save button was clicked');
   }

  componentDidMount(){        
    this.retornoComments(); 
  }


  render(){
    const commentsList = this.state.coments.map(function(coments){
      return (
        <div key={coments.id}>          
          <label className="persona"> {coments.email}</label> <br/><br/><label className="form-control">{coments.body}</label> <br/><br/><br/><br/>
        </div>
      );        
      });
      
        return(
          <div>
            <button type="button" className="btn" onClick={this.openModal}>Ver articulo</button>
            <Modal
            className="Modal__Bootstrap modal-dialog"
            closeTimeoutMS={150}
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.handleModalCloseRequest}
            >
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Articulo:</h4>                         
                <button type="button" className="close" onClick={this.handleModalCloseRequest}>
                <span aria-hidden="true">&times;</span>
                <span className="sr-only">Close</span>
                </button>
              </div>
                <div className="modal-body">   
                  <div className="title">
                    Titulo: {this.props.title}                    
                  </div>
                  <br/>
                  <div className="modalbody_body">
                   Contenido: {this.props.body}                   
                  </div>
                  <br/>
                  <img className="imagen_body" src= "https://via.placeholder.com/150/ecde"></img>
                  <br/>
                  <br/>
                  {commentsList}
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-secondary" onClick={this.handleModalCloseRequest}>Close</button>                 
                </div>
            </div>
            </Modal>
          </div>

        )
    }

};
export default ModalComponent;   

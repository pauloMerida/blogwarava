import React from 'react';
import {Post} from './postComponent.js';
import Api from './Api.js';
import './Api.css';

class Principal extends React.Component{

  constructor(props){
    super(props)
    this.state={
      posts:[]
    } 
  }

  componentDidMount(){
    this.retornoPosts();
  }

  retornoPosts=()=>{
    Api.posts.getList()
    .then(respuesta =>{
      this.setState({
      posts:respuesta
      });
    })
    .catch(error =>{
      console.log(error)    
    })
  }
  render(){
    return(
      <div >  
        <nav>Blog Warva ( {this.state.posts.length} )</nav>       
        <Post posts={this.state.posts}/>         
      </div>
    )
  }
}

export default Principal;

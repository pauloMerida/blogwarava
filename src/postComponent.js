import React from 'react';
import Modal from './ModalComponent.js';
import './Api.css';
import Loader from 'react-loader-spinner';

export class Post extends React.Component{

  render(){
    if(!this.props.posts.length){                
       return(
         <div className="cargando">
           <Loader  type="Oval" color="white" height={80} width={80} />
         </div>
        ) 
    }  
      const postList = this.props.posts.map(function(posts){
      return (
        <div key={posts.id}>
          <div className="card">
            <div className="card-body">          
              <h2>Titulo: { posts.title}</h2>
              <br/>
              <p>{posts.body}</p>
              <div>
                <img src= "https://via.placeholder.com/150/ecde"></img>
                <br/><br/>   
              </div>
              <div className="card-footer">
                <Modal idPosts={posts.id} title={posts.title} body={posts.body} />                 
              </div>
            </div>
          </div>
          <br/> <br/>
        </div>
      );        
    });
    return(
      <div className="col-md-8">        
        <div >                    
          <div className="card-body">
            {postList}
          </div>                    
        </div>                
      </div> 
              
    )
  }
}   

export default Post;
